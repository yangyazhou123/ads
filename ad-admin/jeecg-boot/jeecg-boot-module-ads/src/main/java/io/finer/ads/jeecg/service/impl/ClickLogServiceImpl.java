package io.finer.ads.jeecg.service.impl;

import io.finer.ads.jeecg.entity.ClickLog;
import io.finer.ads.jeecg.mapper.ClickLogMapper;
import io.finer.ads.jeecg.service.IClickLogService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 点击日志
 * @Author: jeecg-boot
 * @Date:   2020-06-16
 * @Version: V1.0
 */
@Service
public class ClickLogServiceImpl extends ServiceImpl<ClickLogMapper, ClickLog> implements IClickLogService {

}
