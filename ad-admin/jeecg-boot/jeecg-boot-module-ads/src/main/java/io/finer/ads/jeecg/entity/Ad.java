package io.finer.ads.jeecg.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 广告
 * @Author: jeecg-boot
 * @Date:   2020-06-16
 * @Version: V1.0
 */
@Data
@TableName("ads_ad")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="ads_ad对象", description="广告")
public class Ad implements Serializable {
    private static final long serialVersionUID = 1L;

	/**ID*/
	@TableId(type = IdType.ID_WORKER_STR)
    @ApiModelProperty(value = "ID")
    private java.lang.String id;
	/**名称*/
	@Excel(name = "名称", width = 15)
    @ApiModelProperty(value = "名称")
    private java.lang.String name;
	/**广告主ID*/
	@Excel(name = "广告主ID", width = 15)
    @ApiModelProperty(value = "广告主ID")
    private java.lang.String advertiserId;
	/**广告主*/
	@Excel(name = "广告主", width = 15)
    @ApiModelProperty(value = "广告主")
    private java.lang.String advertiser;
	/**类型*/
	@Excel(name = "类型", width = 15, dicCode = "ads_ad_type")
	@Dict(dicCode = "ads_ad_type")
    @ApiModelProperty(value = "类型")
    private java.lang.Integer type;
	/**尺寸类型*/
	@Excel(name = "尺寸类型", width = 15)
    @ApiModelProperty(value = "尺寸类型")
    private java.lang.String sizeTypeName;
	/**宽 px*/
	@Excel(name = "宽 px", width = 15)
    @ApiModelProperty(value = "宽 px")
    private java.lang.Integer width;
	/**高 px*/
	@Excel(name = "高 px", width = 15)
    @ApiModelProperty(value = "高 px")
    private java.lang.Integer height;
	/**点击链接*/
	@Excel(name = "点击链接", width = 15)
    @ApiModelProperty(value = "点击链接")
    private java.lang.String clickUrl;
	/**目标窗口*/
	@Excel(name = "目标窗口", width = 15)
    @ApiModelProperty(value = "目标窗口")
    private java.lang.String targetWindow;
	/**文件来源*/
	@Excel(name = "文件来源", width = 15, dicCode = "ads_file_source_type")
	@Dict(dicCode = "ads_file_source_type")
    @ApiModelProperty(value = "文件来源")
    private java.lang.Integer fileSourceType;
	/**广告模板ID*/
	@Excel(name = "广告模板ID", width = 15)
    @ApiModelProperty(value = "广告模板ID")
    private java.lang.String templateId;
	/**内容*/
	@Excel(name = "内容", width = 15)
    @ApiModelProperty(value = "内容")
    private java.lang.String content;
	/**内容扩展*/
	@Excel(name = "内容扩展", width = 15)
    @ApiModelProperty(value = "内容扩展")
    private java.lang.String contentExt;
	/**展示样式*/
	@Excel(name = "展示样式", width = 15)
    @ApiModelProperty(value = "展示样式")
    private java.lang.String style;
	/**启用*/
	@Excel(name = "启用", width = 15, dicCode = "yn")
	@Dict(dicCode = "yn")
    @ApiModelProperty(value = "启用")
    private java.lang.Integer enabled;
	/**备注*/
	@Excel(name = "备注", width = 15)
    @ApiModelProperty(value = "备注")
    private java.lang.String remark;
	/**创建者*/
    @ApiModelProperty(value = "创建者")
    private java.lang.String createBy;
	/**创建时间*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "创建时间")
    private java.util.Date createTime;
	/**修改者*/
    @ApiModelProperty(value = "修改者")
    private java.lang.String updateBy;
	/**修改时间*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "修改时间")
    private java.util.Date updateTime;
}
